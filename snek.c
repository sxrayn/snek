// Copyright 2019 Ravi Bormann
#include "snek.h"
#include "body.h"

const GLfloat RGBA_BLACK[]  = {0, 0, 0, 0};
const GLfloat RGBA_WHITE[]  = {1, 1, 1, 1};
const GLfloat RGBA_RED[]   = {1, 0.3, 0.3, 1};
const GLfloat RGBA_GREEN[] = {0, 1, 0.5, 1};
const GLfloat RGBA_GREEN2[] = {0, 0.6, 0.3, 1};
const GLfloat RGBA_BLUE[]  = {0, 0, 1, 1};
const GLfloat RGBA_GRAY[]  = {0.2, 0.2, 0.2, 1};
const GLfloat *RGBA_COLORS[7] = {
    RGBA_BLACK,
    RGBA_WHITE,
    RGBA_RED,
    RGBA_GREEN,
    RGBA_GREEN2,
    RGBA_BLUE,
    RGBA_GRAY,
};

int plane[PLANE_SIZE][PLANE_SIZE];
int fx, fy, fc, dir, overflow, next_dir, score;
int last_x, last_y;
bool running, moved;
snake_t *snake;

void gameInit() {
    srand(time(NULL));
    for (int y = 0; y < PLANE_SIZE; y++) {
        for (int x = 0; x < PLANE_SIZE; x++) {
            plane[x][y] = PLANE;
        }
    }

    fc = 0;
    overflow = 0;
    dir = NORTH;
    next_dir = -1;
    snake = initSnake(PLANE_SIZE / 2, PLANE_SIZE / 2);
    snake->next = initSnake(PLANE_SIZE / 2, PLANE_SIZE / 2 - 1);
    running = false;
}

void gameTick() {
    snake_t *next;

    for (int y = 0; y < PLANE_SIZE; y++) {
        for (int x = 0; x < PLANE_SIZE; x++) {
            plane[x][y] = PLANE;
        } }
    plane[fx][fy] = FOOD;

    if (!fc) {
        fx = rand() % PLANE_SIZE;
        fy = rand() % PLANE_SIZE;
        fc++;
    }

    snake = snakeMove(snake, dir, overflow);
    if (overflow > 0) overflow--;
    plane[snake->x][snake->y] = HEAD;

    next = snake->next;
    while (next) {
        plane[next->x][next->y] = SNEK;
        next = next->next;
    }

    if ((snake->x < 0 || snake->x >= PLANE_SIZE) || (snake->y < 0 || snake->y >= PLANE_SIZE)) {
        printf("Game over!\n");
        gameOver();
    } else if (plane[snake->x][snake->y] == SNEK) {
        printf("Game over!\n");
        gameOver();
    } else if (snake->x == fx && snake->y == fy) {
        score++;
        overflow++;
        fc--;
    }
}

void gameOver() {
    destroySnake(snake);
    gameInit();
}

void draw(GLFWwindow* window) {
    int width, height;
    float ratio;

    glfwGetFramebufferSize(window, &width, &height);
    ratio = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    glScalef(.05f, .05f, .05f);
    glTranslatef(-PLANE_SIZE / 2, -PLANE_SIZE / 2, 0);
    
    for (int y = 0; y < PLANE_SIZE; y++) {
        for (int x = 0; x < PLANE_SIZE; x++) {
            drawSquare(x, y, 0.9f, RGBA_COLORS[plane[x][y]]);
        }
    }

    glFlush();
    glfwSwapBuffers(window);
}

void drawSquare(const float x, const float y, const float d, const GLfloat *color) {
    float r = d / 2;
    GLfloat square[] = {
        x - r, y - r,
        x + r, y - r,
        x - r, y + r,
        x + r, y + r
    };
    glColor4f(color[0], color[1], color[2], color[3]);
    glVertexPointer(2, GL_FLOAT, 0, square);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void errorCallback(int error, const char *description) {
    fputs(description, stderr);
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (last_x != snake->x || last_y != snake->y) {
       moved = false;
    } 
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, GL_TRUE);
                break;
            case GLFW_KEY_SPACE:
                running = !running;
                break;
            case GLFW_KEY_W:
                if (dir != SOUTH && !moved) {
                    dir = NORTH;
                    moved = true;
                }
                break;
            case GLFW_KEY_S:
                if (dir != NORTH && !moved) {
                    dir = SOUTH;
                    moved = true;
                }
                break;
            case GLFW_KEY_D:
                if (dir != WEST && !moved) {
                    dir = EAST;
                    moved = true;
                }
                break;
            case GLFW_KEY_A:
                if (dir != EAST && !moved) {
                    dir = WEST;
                    moved = true;
                }
                break;
        }
    }
    last_x = snake->x;
    last_y = snake->y;
}

void mouseCallback(GLFWwindow *window, int button, int action, int mods) {
}

int main(int argc, char **argv) {
    GLFWwindow* window;
    glfwSetErrorCallback(errorCallback);
    if (!glfwInit()) {
        return -1;
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 1);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    window = glfwCreateWindow(H_RES, V_RES, "snek", 0, 0);
    if (!window) {
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    printf("Renderer: %s\n", glGetString(GL_RENDERER));
    printf("Version: %s\n", glGetString(GL_VERSION));

    glfwSetKeyCallback(window, keyCallback);
    glfwSetMouseButtonCallback(window, mouseCallback);
    glEnableClientState(GL_VERTEX_ARRAY);
    gameInit();

    while (!glfwWindowShouldClose(window)) {
        if (running) {
            if (next_dir > 0) {
                dir = next_dir;
                next_dir = -1;
            }
            gameTick();
        }
        draw(window);
        glfwPollEvents();
        usleep(GAME_TICK);
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
