CC = gcc
CFLAGS = -I. -Wall -pedantic -g -fsanitize=address -fsanitize=undefined --std=c11

all: compile clean test

compile: snek

test:
	./snek

clean:
	rm -f *.o

snek: snek.o body.o
	$(CC) $(CFLAGS) $^ -lGLESv1_CM -lglfw -o $@ -lm

