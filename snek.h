// Copyright 2019 Ravi Bormann
#ifndef SNEK_H_
#define SNEK_H_

#include <GLES/gl.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define H_RES 1024
#define V_RES 768
#define GAME_TICK 100000
#define PLANE_SIZE 32

#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3

#define FOOD 2
#define SNEK 4
#define HEAD 3
#define PLANE 6

void gameInit();
void gameTick();
void gameOver();
void draw(GLFWwindow* window);
void drawSquare(const float x, const float y, const float d, const GLfloat *color);
void errorCallback(int error, const char *description);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouseCallback(GLFWwindow *window, int button, int action, int mods);
int main(int argc, char **argv);

#endif  // SNEK_H_
