def Settings( **kwargs ):
  return {
    'flags': ['-I.',  '-Wall', '-pedantic', '-g', '-fsanitize=address', '-fsanitize=undefined', '--std=c11'],
  }
