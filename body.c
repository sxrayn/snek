// Copyright 2019 Ravi Bormann
#include "body.h"
#include "snek.h"


snake_t *initSnake(int x, int y) {
    snake_t *s = malloc(sizeof(snake_t));
    s->x = x;
    s->y = y;
    s->next = NULL;
    return s;
}

snake_t *copySnake(snake_t *s) {
    snake_t *new = malloc(sizeof(snake_t));
    new->x = s->x;
    new->y = s->y;
    new->next = s->next;
    return new;
}

snake_t *snakeMove(snake_t *s, int direction, int overflow) {
    snake_t *head = copySnake(s);
    snake_t *tmp;

    switch (direction) {
        case NORTH:
            if (head->y < 31)
                head->y = s->y + 1;
            break;
        case SOUTH:
            if (head->y > 0)
                head->y = s->y - 1;
            break;
        case EAST:
            if (head->x < 31)
                head->x = s->x + 1;
            break;
        case WEST:
            if (head->x > 0)
                head->x = s->x - 1;
            break;
    }

    if (overflow || snakeHasNext(head)) {
        head->next = s;
    }

    if (!overflow && snakeHasNext(s)) {
        tmp = head->next;
        while(!snakeIsSecondLast(tmp)) {
            tmp = tmp->next;
        }
        destroySnake(tmp->next);
        tmp->next = NULL;
    } else {
        overflow--;
    }
    return head;
}

bool snakeIsSecondLast(snake_t *s) {
    if(s->next->next) return false;
    else return true;
}

bool snakeHasNext(snake_t *s) {
    if(s->next) return true;
    else return false;
}

void destroySnake(snake_t *s) {
    snake_t *tmp = s->next;
    snake_t *tmp2;
    while(tmp) {
        tmp2 = tmp->next;
        free(tmp);
        tmp = tmp2;
    }
    free(s);
}
