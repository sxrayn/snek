// Copyright 2019 Ravi Bormann
#ifndef BODY_H_
#define BODY_H_

#include <stdlib.h>
#include <stdbool.h>

typedef struct _snake_t snake_t;

struct _snake_t {
    int x;
    int y;
    snake_t *next;
};

snake_t *initSnake(int x, int y);
snake_t *copySnake(snake_t *s);
snake_t *snakeMove(snake_t *s, int direction, int overflow);
bool snakeIsSecondLast(snake_t *s);
bool snakeHasNext(snake_t *s);
void destroySnake(snake_t *s);

#endif  // BODY_H_
